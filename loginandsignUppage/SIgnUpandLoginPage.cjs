
function toggleLoaderInDocuement() {
    const loader = document.querySelector(".loader");
    if (loader.style.display === "none") {

        loader.style.display = "block"
    } else {

        loader.style.display = "none";
    }
}


function dispaySomethingWentWrong() {
    const errorDisplay = document.querySelector(".we-failed");
    errorDisplay.style.display = "inline";

}

const validEmailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const vaildPasswordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
const validNameRegex = /^[a-zA-Z]*$/;

const form = document.getElementById("signUpForm");
const firstName = document.getElementById("first-name");
const lastName = document.getElementById("last-name");
const email = document.getElementById("email");
const password = document.getElementById("password");
const repeatPassword = document.getElementById("repeat-password");
const tosCheckbox = document.getElementById("tosCheckbox");
const signUpHeader = document.querySelector(".signupheader");
const showToslink = document.getElementById("tos");
const termsAndConditions = document.querySelector(".termsxcondition");
const closeTermsxCondition = document.getElementById("colse-tos");


showToslink.addEventListener("click", (event) => {
    signUpHeader.style.display = "none";
    form.style.display = "none";
    termsAndConditions.style.display = "block";
});

closeTermsxCondition.addEventListener("click", (event) => {
    termsAndConditions.style.display = "none";
    signUpHeader.style.display = "block";
    form.style.display = "flex";
});



form.addEventListener('submit', (event) => {

    event.preventDefault();
    let isFormValid = true;

    removeErrorMessage(firstName);
    if (firstName.value.length === 0 || validNameRegex.test(firstName.value) != true) {

        isFormValid = false;
        if (firstName.value.length === 0) {
            displayErrorMessage(firstName, "You left first name empty, enter no-space alphabets only name");

        } else {
            displayErrorMessage(firstName, "Invalid first name, enter no-space alphabets only name")
        }


    }

    removeErrorMessage(lastName);
    if (lastName.value.length === 0 || validNameRegex.test(lastName.value) != true) {

        isFormValid = false;
        if (lastName.value.length === 0) {
            displayErrorMessage(lastName, "You left last name empty, enter no-space alphabets only name");

        } else {
            displayErrorMessage(lastName, "Invalid first name, enter no-space alphabets only name");
        }

    }

    removeErrorMessage(email);
    if (email.value.length === 0 || validEmailRegex.test(email.value) != true) {

        isFormValid = false;
        if (email.value.length === 0) {
            displayErrorMessage(email, "You left email empty, enter something like username@domain");

        } else {
            displayErrorMessage(email, "Invalid first name, enter something like username@domain");
        }

    }

    removeErrorMessage(password);
    if (password.value.length === 0 || vaildPasswordRegex.test(password.value) != true) {

        isFormValid = false;
        if (password.value.length === 0) {
            displayErrorMessage(password, "You left password empty, password should have Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character");

        } else {
            displayErrorMessage(password, "Invalid password, password should have Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character");
        }

    }

    removeErrorMessage(repeatPassword);
    if (repeatPassword.value.length === 0 || repeatPassword.value !== password.value) {
        
        isFormValid = false;
        if (repeatPassword.value.length === 0) {
            displayErrorMessage(repeatPassword, "You left repeat password empty, this should match above password");

        } else {
            displayErrorMessage(repeatPassword, "Password missmatch");
        }
    }

    removeErrorMessage(tosCheckbox);
    if (tosCheckbox.checked === false) {

        isFormValid = false;
        displayErrorMessage(tosCheckbox, "Please agree to our Terms and Conditions");

    }

    if (isFormValid == false) {
        signUpHeader.children[0].textContent = "Sign Up failed! Invalid inputs, check our suggestions";
    } else {
        signUpHeader.children[0].textContent = "Sign Up sucessfull check your email";
        //Some call to api to send signup form data
    }

});



function displayErrorMessage(inputElement, messageString) {
    inputElement.style.border = '2px solid red';
    const errorMessagePElement = inputElement.parentElement.querySelector('.invalid-warning');
    errorMessagePElement.textContent = messageString;

}

function removeErrorMessage(inputElement) {
    inputElement.style.border = '2px solid green';
    const errorMessagePElement = inputElement.parentElement.querySelector('.invalid-warning');
    errorMessagePElement.textContent = "";

}

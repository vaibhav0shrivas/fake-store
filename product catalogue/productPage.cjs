const productsURL = 'https://fakestoreapi.com/products/';

function returnLiElement(productObject) {

    let newlielement = document.createElement("li");
    newlielement.className = "listItem";
    newlielement.innerHTML =
        `<div class="productCard">
        <div class="image-container">
            <img src="${productObject.image}" alt="${productObject.description}">
        </div>
        <br>
        <h3>${productObject.title}</h3>
        <div>
            <div class="rating">Rating: <span>${productObject.rating.rate}</span></div>
            <div class="price">Price: <span>&dollar;${productObject.price}</span></div>
        </div>
        <div class buttons>
            <button class="buy button">Buy</button>
            <button class="add-to-cart button">Add to cart</button>
        </div>
    </div>`
    return newlielement;

}

function removeLoaderFromDocuement() {
    const loader = document.querySelector(".loader");
    loader.remove();
}

function dispayFailedAPIcallInDocuement() {
    const errorDisplay = document.querySelector(".we-failed");
    errorDisplay.style.display = "inline";

}
function dispayNoProductsInDocuement() {
    const errorDisplay = document.querySelector(".no-products");
    errorDisplay.style.display = "inline";

}


fetch(productsURL)
    .then((response) => {

        if (!response.ok) {
            throw Error('Fetch Failed');
        }
        return response.json();

    })
    .then((data) => {
        removeLoaderFromDocuement();

        if (data.length == 0) {
            dispayNoProductsInDocuement();

        } else {
            const uL = document.getElementById("productList");
            data.map(productObject => {
                uL.appendChild(returnLiElement(productObject));
                return null;
            });
        }
    })
    .catch((error) => {
        removeLoaderFromDocuement();
        dispayFailedAPIcallInDocuement();
        console.error(error);
    });
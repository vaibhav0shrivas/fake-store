const usersURL = 'https://fakestoreapi.com/users';

function returnLiElement(userObject) {

    let newlielement = document.createElement("li");
    newlielement.className = "listItem";

    let newUserCardDiv = document.createElement("div");
    newUserCardDiv.className = "UserCard";

    let newh3ForUserFullName = document.createElement("h3");
    newh3ForUserFullName.className="user-full-name";
    newh3ForUserFullName.textContent = `${userObject.name.firstname} ${userObject.name.lastname}`;

    let newh4ForUsername = document.createElement("h4");
    newh4ForUsername.textContent = `user-name : ${userObject.username}`;

    let newUserDataDiv = document.createElement("div");
    let newDivForAddress = document.createElement("div");
    newDivForAddress.className = 'address';
    newDivForAddress.textContent = `Address : #${userObject.address.number} ${userObject.address.street}, ${userObject.address.city}, Zipcode=> ${userObject.address.zipcode}`;
    let newDivForPhone = document.createElement("div");
    newDivForPhone.className = 'phone'
    newDivForPhone.textContent = `Phone : ${userObject.phone}`;
    newUserDataDiv.append(newDivForAddress, newDivForPhone);

    let newDivForButtons = document.createElement("div");
    newDivForButtons.className = "buttons";
    let newButton = document.createElement("button");
    newButton.classList = ["more", "button"];
    newButton.textContent = `More`
    newDivForButtons.append(newButton);

    newUserCardDiv.append(newh3ForUserFullName, newh4ForUsername, newUserDataDiv, newDivForButtons);
    newlielement.append(newUserCardDiv);

    return newlielement;

}

function removeLoaderFromDocuement() {
    const loader = document.querySelector(".loader");
    loader.remove();
}

function dispayFailedAPIcallInDocuement() {
    const errorDisplay = document.querySelector(".we-failed");
    errorDisplay.style.display = "inline";

}
function dispayNoUsersInDocuement() {
    const errorDisplay = document.querySelector(".no-users");
    errorDisplay.style.display = "inline";

}


fetch(usersURL)
    .then((response) => {

        if (!response.ok) {
            throw Error('Fetch Failed');
        }
        return response.json();

    })
    .then((data) => {
        removeLoaderFromDocuement();

        if (data.length == 0) {
            dispayNoUsersInDocuement();

        } else {
            const uL = document.getElementById("UserList");
            data.forEach(userObject => {
                uL.appendChild(returnLiElement(userObject));
            });
        }
    })
    .catch((error) => {
        removeLoaderFromDocuement();
        dispayFailedAPIcallInDocuement();
        console.error(error);
    });
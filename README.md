# Fake Store

## API [https://fakestoreapi.com/](https://fakestoreapi.com/)

## Task 1

Load the products from given api in the frontend and render them on the screen.
Make the UI as nice as possible.

## Task 2

Render the users on a different page.
[https://fakestoreapi.com/users](https://fakestoreapi.com/users)

# Task 3

Add a signup page.
